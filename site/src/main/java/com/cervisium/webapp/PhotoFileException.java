package com.cervisium.webapp;

public class PhotoFileException extends Exception {

	public PhotoFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public PhotoFileException(String message) {
		super(message);
	}

}
