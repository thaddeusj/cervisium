package com.cervisium.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BREWING_NOTES")
public class BrewingNotes {

	private Integer brewingId;
	private String brewNotes = "";
	private String tasteNotes = "";
	
	@Id
	@Column(name = "BREWING_ID")
	public Integer getBrewingId() {
		return brewingId;
	}
	
	public void setBrewingId(Integer brewingId) {
		this.brewingId = brewingId;
	}

	@Column(name = "BREW_NOTES")
	public String getBrewNotes() {
		return brewNotes;
	}

	public void setBrewNotes(String brewNotes) {
		this.brewNotes = brewNotes;
	}

	@Column(name = "TASTE_NOTES")
	public String getTasteNotes() {
		return tasteNotes;
	}

	public void setTasteNotes(String tasteNotes) {
		this.tasteNotes = tasteNotes;
	}
}
