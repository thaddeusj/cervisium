package com.cervisium.webapp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "RECIPE")
public class Recipe implements IntegerKeyedEntity {

	private Integer id;
	private Integer breweryId;
	private String name;
	private String slugName;
	private String style;
	private Float targetOg;
	private Float targetFg;
	private Integer targetIBU;
	private Integer targetSRM;
	private Integer referenceVersion;
	private List<Brewer> authors = new ArrayList<Brewer>();
	private List<Formulation> formulations = new ArrayList<Formulation>();
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "BREWERY_ID")
	@NotNull
	public Integer getBreweryId() {
		return breweryId;
	}

	public void setBreweryId(Integer breweryId) {
		this.breweryId = breweryId;
	}
	
	@Column(name = "NAME")
	@NotNull
	@Size(min = 1, max = 255)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "SLUG_NAME")
	public String getSlugName() {
		return slugName;
	}

	public void setSlugName(String slugName) {
		this.slugName = slugName;
	}

	@Column(name = "STYLE")
	@NotNull
	@Size(min = 1, max = 255)
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	@Column(name = "TARGET_OG")
	@NotNull
	public Float getTargetOg() {
		return targetOg;
	}

	public void setTargetOg(Float targetOg) {
		this.targetOg = targetOg;
	}
	
	@Column(name = "TARGET_FG")
	@NotNull
	public Float getTargetFg() {
		return targetFg;
	}

	public void setTargetFg(Float targetFg) {
		this.targetFg = targetFg;
	}

	@Column(name = "TARGET_IBU")
	@NotNull
	public Integer getTargetIBU() {
		return targetIBU;
	}

	public void setTargetIBU(Integer targetIBU) {
		this.targetIBU = targetIBU;
	}

	@Column(name = "TARGET_SRM")
	@NotNull
	public Integer getTargetSRM() {
		return targetSRM;
	}

	public void setTargetSRM(Integer targetSRM) {
		this.targetSRM = targetSRM;
	}

	@Column(name = "REFERENCE_VERSION")
	public Integer getReferenceVersion() {
		return referenceVersion;
	}

	public void setReferenceVersion(Integer referenceVersion) {
		this.referenceVersion = referenceVersion;
	}
	
	@ManyToMany(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "RECIPE_AUTHOR_MAPPING",
		joinColumns = @JoinColumn(name = "RECIPE_ID", referencedColumnName = "ID"),
		inverseJoinColumns = @JoinColumn(name = "BREWER_ID", referencedColumnName = "ID"))
	public List<Brewer> getAuthors() {
		return authors;
	}
	
	public void setAuthors(List<Brewer> authors) {
		this.authors = authors;
	}
	
	@OneToMany(mappedBy = "recipe", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
	public List<Formulation> getFormulations() {
		return formulations;
	}
	
	public void setFormulations(List<Formulation> formulations) {
		this.formulations = formulations;
	}
}
