package com.cervisium.webapp.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.RecipeFileLocation;
import com.cervisium.webapp.entity.Brewing;
import com.cervisium.webapp.entity.Formulation;

@WebServlet(name = "RecipeDownloadServlet", urlPatterns = { "/downloadRecipe/formulation", "/downloadRecipe/brewing" })
public class RecipeDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(unitName = "CervisiumDS") private transient EntityManager em;
	@Inject @RecipeFileLocation private File recipeFileLocation;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecipeDownloadServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idStr = request.getParameter("id");
		if (idStr == null || idStr.length() == 0) { response.setStatus(404); return; }
		Integer id = -1;
		try { id = Integer.parseInt(idStr); }
		catch (NumberFormatException nfe) { response.setStatus(404); return; }
		
		// Handle formulation recipe files
		if (request.getServletPath().endsWith("formulation")) {
			Formulation formulation = em.find(Formulation.class, id);
			if (formulation == null) { response.setStatus(404); return; }
			
			File formulationFile = new File(formulation.getFilePath());
			if (formulationFile.exists() == false) { response.setStatus(404); return; }
			
			String fileName = formulation.createFriendlyFileName() + ".btp";
			
			response.setStatus(200);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			
			FileInputStream formulaIn = null;
			try {
				formulaIn = new FileInputStream(formulationFile);
				IOUtils.copy(formulaIn, response.getOutputStream());
			} finally {
				if (formulaIn != null) { try { formulaIn.close(); } catch (IOException ioe2) {} }
			}
		} else if (request.getServletPath().endsWith("brewing")) {
			Brewing brewing = em.find(Brewing.class, id);
			if (brewing == null) { response.setStatus(404); return; }
			
			File brewingFile = new File(recipeFileLocation, "brewing-" + Integer.toString(brewing.getId()) + ".btp");
			if (brewingFile.exists() == false) { response.setStatus(404); return; }
			
			String fileName = brewing.getFriendlyName() + ".btp";
			
			response.setStatus(200);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			
			FileInputStream formulaIn = null;
			try {
				formulaIn = new FileInputStream(brewingFile);
				IOUtils.copy(formulaIn, response.getOutputStream());
			} finally {
				if (formulaIn != null) { try { formulaIn.close(); } catch (IOException ioe2) {} }
			}
		}
		
	}

}
