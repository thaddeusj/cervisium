package com.cervisium.webapp;

import java.io.File;
import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.cervisium.webapp.btp.BTPData;
import com.cervisium.webapp.btp.BrewingData;
import com.cervisium.webapp.entity.Brewing;

@ApplicationScoped
public class BrewingDataImporter {

	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject @RecipeFileLocation private File recipeFileLocation;
	
	public void copyFromBTP(Brewing brewing) throws BrewingDataImportException {
		File brewingFile = new File(recipeFileLocation, "brewing-" + Integer.toString(brewing.getId()) + ".btp");
		
		if (brewingFile.exists() == false) {
			throw new BrewingDataImportException("Brewing file does not exist: " + brewingFile.getAbsolutePath());
		}
		
		BTPData btp = new BTPData(brewingFile);
		try {
			btp.loadDataFile();
		} catch (IOException ioe) {
			throw new BrewingDataImportException("I/O Exception parsing brewing data file", ioe);
		}
		
		BrewingData data = btp.getParser().getBrewingData();
		if (data.getOg() != null) { brewing.setOg(data.getOg()); }
		if (data.getFg() != null) { brewing.setFg(data.getFg()); }
		if (data.getEfficiency() != null) { brewing.setEfficiency(data.getEfficiency()); }
		if (data.getAttenuationApp() != null) { brewing.setAttenuationApp(data.getAttenuationApp()); }
		if (data.getMashTempF() != null) { brewing.setMashTempF(data.getMashTempF()); }
	}

}
