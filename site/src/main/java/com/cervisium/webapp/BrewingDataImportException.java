package com.cervisium.webapp;

public class BrewingDataImportException extends Exception {
	public static final long serialVersionUID = 1l;

	public BrewingDataImportException(String message, Throwable cause) {
		super(message, cause);
	}

	public BrewingDataImportException(String message) {
		super(message);
	}
}
