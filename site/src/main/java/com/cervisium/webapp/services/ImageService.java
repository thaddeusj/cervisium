package com.cervisium.webapp.services;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.File;
import java.io.IOException;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.imgscalr.Scalr;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.PhotoFileLocation;
import com.cervisium.webapp.entity.Album;
import com.cervisium.webapp.entity.Photo;

@Stateless
public class ImageService {

	@PersistenceContext(unitName = "CervisiumDS") private EntityManager em;
	@Inject @Cervisium private Logger log;
	@Inject @PhotoFileLocation File photoFileLocation;
	
	/**
	 * <p>Generates a 128x96 thumbnail and saves it in the album directory with the prefix of "tn-".</p>
	 * <p>Portrait orientation pictures will be resized to 128px wide then cropped to 96px in height.</p>
	 * @param photo Photo for which tumbnail is to be generated
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Asynchronous
	public void generateThumb(Photo photo) {
		log.info("Generating thumbnail for " + photo.getFileName() + " in album " + photo.getAlbum().getName());
		
		File photoFile = getFileForPhoto(photo);
		
		String thumbFileName = "tn-" + photo.getFileName();
		thumbFileName.replaceFirst("\\.(png|PNG)$", ".jpg");
		File thumbFile = new File(photoFile.getParentFile(), thumbFileName);
		
		BufferedImage original = null;
		BufferedImage thumb = null;
		
		try {
			original = ImageIO.read(photoFile);
			thumb = Scalr.resize(original, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH, 128, new BufferedImageOp[0]);
			if (thumb.getHeight() > 96) { thumb = Scalr.crop(thumb, 128, 96, new BufferedImageOp[0]); }
			ImageIO.write(thumb, "JPEG", thumbFile);
		} catch (IOException ioe) {
			log.error("Error generating thumbnail", ioe);
			throw new ThumbnailGenerationException("Error generating thumbnail", ioe);
		}
		
		photo.setThumbnail(thumbFileName);
		em.merge(photo);
	}
	
	@Asynchronous
	public void rethumbAlbum(Album album) {
		album = em.find(Album.class, album.getId());
		for (Photo p : album.getPhotos()) {
			generateThumb(p);
		}
	}
	
	@Asynchronous
	public void resizePhoto(Photo photo, int maxDim) {
		log.info("Resizing photo " + photo.getFileName() + " in album " + photo.getAlbum().getName() +
			" with max dimension " + Integer.toString(maxDim));
		
		File photoFile = getFileForPhoto(photo);
		BufferedImage original = null;
		BufferedImage resized = null;
		
		try {
			original = ImageIO.read(photoFile);
			resized = Scalr.resize(original, Scalr.Method.QUALITY, maxDim, new BufferedImageOp[0]);
			ImageIO.write(resized, "JPEG", photoFile);
		} catch (IOException ioe) {
			log.error("Error resizing image", ioe);
			throw new ThumbnailGenerationException("Error resizing image", ioe);
		}
	}
	
	protected File getFileForPhoto(Photo photo) {
		File albumFolder = new File(photoFileLocation, photo.getAlbum().getFolder());
		File photoFile = new File(albumFolder, photo.getFileName());
		return photoFile;
	}
}
