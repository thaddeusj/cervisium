package com.cervisium.webapp.services;

public class ThumbnailGenerationException extends RuntimeException {
	private static final long serialVersionUID = 1l;

	public ThumbnailGenerationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ThumbnailGenerationException(String message) {
		super(message);
	}
}
