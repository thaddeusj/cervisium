package com.cervisium.webapp.services;

import java.io.File;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.PhotoFileLocation;
import com.cervisium.webapp.entity.Album;
import com.cervisium.webapp.entity.Photo;

@Stateless
public class PhotoDeleteService {
	@PersistenceContext(unitName = "CervisiumDS") private EntityManager em;
	@Inject @Cervisium private Logger log;
	@Inject @PhotoFileLocation File photoFileLocation;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deletePhoto(Photo photo) {
		em.detach(photo);
	
		File albumFolder = new File(photoFileLocation, photo.getAlbum().getFolder());
		File photoFile = new File(albumFolder, photo.getFileName());
		
		photoFile.delete();
		if (photo.getThumbnail() != null) {
			File thumbFile = new File(albumFolder, photo.getThumbnail());
			thumbFile.delete();
		}
		
		em.createQuery("DELETE FROM Photo p WHERE p.id = :id")
			.setParameter("id", photo.getId()).executeUpdate();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteAlbum(Album album) {
		album = em.find(Album.class, album.getId());
		
		File albumFolder = new File(photoFileLocation, album.getFolder());
		
		for (Photo photo : album.getPhotos()) {
			deletePhoto(photo);
		}
		
		albumFolder.delete();
		
		em.createQuery("DELETE FROM Album a WHERE a.id = :id")
			.setParameter("id", album.getId()).executeUpdate();
	}
}
