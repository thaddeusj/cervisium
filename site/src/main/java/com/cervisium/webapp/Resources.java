package com.cervisium.webapp;

import java.io.File;

import javax.annotation.Resource;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.apache.log4j.Logger;
import org.jboss.solder.core.ExtensionManaged;

public class Resources {
	private static Logger log = Logger.getLogger(Resources.class);
	
	@ExtensionManaged @ConversationScoped
	@Produces @Cervisium
	@PersistenceUnit(unitName = "CervisiumDS")
	private static EntityManagerFactory emf;
	
	@Produces @Cervisium
	public static Logger getLogger(InjectionPoint ip) {
		return Logger.getLogger(ip.getMember().getDeclaringClass());
	}
	
	@Produces @RecipeFileLocation
	public static File getRecipeFileLocation() {
		String locPath = System.getProperty("com.cervisium.recipeFileLocation", null);
		if (locPath != null) {
			File locFile = new File(locPath);
			if (locFile.isDirectory()) { return locFile; }
			else { log.error("com.cervisium.recipeFileLocation specifies a path that is not a directory"); }
		} else {
			log.error("com.cervisium.recipeFileLocation not set");
		}
		
		return null;
	}
	
	@Produces @PhotoFileLocation
	public static File getPhotoFileLocation() {
		String locPath = System.getProperty("com.cervisium.photoFileLocation", null);
		if (locPath != null) {
			File locFile = new File(locPath);
			if (locFile.isDirectory()) { return locFile; }
			else { log.error("com.cervisium.photoFileLocation specifies a path that is not a directory"); }
		} else {
			log.error("com.cervisium.photoFileLocation not set");
		}
		
		return null;
	}
	
	public static String getPhotoURLPrefix() {
		return System.getProperty("com.cervisium.photoURLPrefix", "/photo/");
	}
}
