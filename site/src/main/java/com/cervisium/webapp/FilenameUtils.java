package com.cervisium.webapp;

public class FilenameUtils {

	public static String simplifyFilename(String filename) {
		String simpleName = "";
		
		simpleName = filename.replaceAll("\\s", "_");
		simpleName = simpleName.replaceAll("[^a-zA-Z0-9_\\-\\.]", "");
		
		return simpleName;
	}
}
