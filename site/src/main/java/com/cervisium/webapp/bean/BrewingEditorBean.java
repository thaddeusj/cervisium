package com.cervisium.webapp.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

import com.cervisium.webapp.BrewingDataImportException;
import com.cervisium.webapp.BrewingDataImporter;
import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.RecipeFileLocation;
import com.cervisium.webapp.entity.Brewing;
import com.cervisium.webapp.entity.BrewingNotes;
import com.cervisium.webapp.services.RecipeDeleteService;

@Named("brewingEditor")
@ViewScoped
public class BrewingEditorBean implements Serializable {
	
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject @RecipeFileLocation private File recipeFileLocation;
	@Inject private transient BrewingDataImporter importer;
	@Inject private SessionState session;
	@Inject private RecipeDeleteService deleteService;
	
	private Integer editBrewingId;
	private Brewing editedBrewing;
	private BrewingNotes brewingNotes;
	
	private File brewingFile;
	private Boolean importNewValues = false;
	
	public Integer getEditBrewingId() {
		return editBrewingId;
	}
	
	public void setEditBrewingId(Integer editBrewingId) {
		this.editBrewingId = editBrewingId;
	}
	
	public Brewing getEditedBrewing() {
		return editedBrewing;
	}
	
	public void setEditedBrewing(Brewing editedBrewing) {
		this.editedBrewing = editedBrewing;
	}
	
	public Boolean getBrewingFileExists() {
		return brewingFile.exists();
	}
	
	public Boolean getImportNewValues() {
		return importNewValues;
	}
	
	public void setImportNewValues(Boolean importNewValues) {
		this.importNewValues = importNewValues;
	}
	
	public BrewingNotes getBrewingNotes() {
		return brewingNotes;
	}
	
	public void setBrewingNotes(BrewingNotes brewingNotes) {
		this.brewingNotes = brewingNotes;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void loadBrewing() {
		editedBrewing = em.find(Brewing.class, editBrewingId);
		em.detach(editedBrewing);
		
		brewingNotes = em.find(BrewingNotes.class, editedBrewing.getId());
		if (brewingNotes == null) {
			brewingNotes = new BrewingNotes();
			brewingNotes.setBrewingId(editedBrewing.getId());
		}
		
		brewingFile = new File(recipeFileLocation, "brewing-" + Integer.toString(editedBrewing.getId()) + ".btp");
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveBrewing() {
		if (getWritable() == false) { return; }
		em.merge(editedBrewing);
		em.merge(brewingNotes);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Brewing record saved.", null));
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void refreshBrewing() {
		editedBrewing = em.find(Brewing.class, editedBrewing.getId());
		brewingNotes = em.find(BrewingNotes.class, editedBrewing.getId());
		if (brewingNotes == null) {
			brewingNotes = new BrewingNotes();
			brewingNotes.setBrewingId(editedBrewing.getId());
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String deleteBrewing() {
		if (getWritable() == false) { return ""; }
		
		String idStr = Integer.toString(editedBrewing.getFormulation().getId());
		deleteService.deleteBrewings(editedBrewing);
		return "/recipes/formulation?faces-redirect=true&id=" + idStr;
	}
	
	public void uploadListener(FileUploadEvent event) {
		if (getWritable() == false) {
			log.error("Brewing file uploaded by user without write access.");
			return;
		}
		
		FileOutputStream fout = null;
		
		try {
			fout = new FileOutputStream(brewingFile);
			fout.write(event.getFile().getContents());
		} catch (IOException ioe) {
			log.error("Failed to save upload to location: " + brewingFile.getAbsolutePath(), ioe);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upload failed.", null));
		} finally {
			try { fout.close(); } catch (IOException ioe2) {}
		}
		
		String message = "New brewing file uploaded.";
		
		if (importNewValues) {
			try {
				importer.copyFromBTP(editedBrewing);
				em.merge(editedBrewing);
				message += " Importing new values from uploaded file.";
			} catch (BrewingDataImportException bdie) {
				log.error("Import of brewing data failed", bdie);
				message += " Importing of new values failed.";
			}
			
		} else {
			message += " NOT Importing new values.";
		}
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}
	
	public boolean getWritable() {
		if (session.getLoggedIn()) {
			if (session.getLoggedInUser().getBrewery().getId() == editedBrewing.getFormulation().getRecipe().getBreweryId()) {
				return true;
			}
		}
		
		return false;
	}
}
