package com.cervisium.webapp.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.bean.model.InventoryItem;
import com.cervisium.webapp.entity.Brewing;

@Named("inventory")
@RequestScoped
public class InventoryBean {

	@Inject @Cervisium private EntityManager em;

	private ArrayList<InventoryItem> inventoryItems;
	
	public ArrayList<InventoryItem> getInventoryItems() {
		return inventoryItems;
	}
	
	public void setInventoryItems(ArrayList<InventoryItem> inventoryItems) {
		this.inventoryItems = inventoryItems;
	}
	
	@PostConstruct
	public void init() {
		inventoryItems = new ArrayList<InventoryItem>();
		
		List<Brewing> brewings = em.createQuery("SELECT b FROM Brewing b where dateFinish = NULL", Brewing.class)
			.getResultList();
		
		for (Brewing brewing : brewings) {
			InventoryItem item = new InventoryItem();
			item.setName(brewing.getFormulation().getRecipe().getName() + " - " + brewing.getFormulation().getName());
			item.setBrewingId(brewing.getId());
			item.setDateBrew(brewing.getDateBrew());
			item.setDateSecondary(brewing.getDateSecondary());
			item.setDateKeg(brewing.getDateKeg());
			item.setDateBottle(brewing.getDateBottle());
			item.setStage(brewing.getBrewStage());
			
			Days age = Days.daysBetween(new DateTime(brewing.getDateBrew()), new DateTime());
			item.setAgeDays(age.getDays());
			
			inventoryItems.add(item);
		}
		
		Collections.sort(inventoryItems);
	}
}
