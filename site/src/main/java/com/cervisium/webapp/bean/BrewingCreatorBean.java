package com.cervisium.webapp.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

import com.cervisium.webapp.BrewingDataImportException;
import com.cervisium.webapp.BrewingDataImporter;
import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.RecipeFileLocation;
import com.cervisium.webapp.entity.Brewing;
import com.cervisium.webapp.entity.Formulation;

@ViewScoped
@Named("brewingCreator")
public class BrewingCreatorBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject @RecipeFileLocation private File recipeFileLocation;
	@Inject private transient BrewingDataImporter importer;
	@Inject private SessionState session;
	
	private Integer formulationId;
	private Brewing newBrewing;
	
	private Boolean importButtonDisabled;
	private Boolean createButtonDisabled;
	
	private File tempBrewingFile;
	
	public Integer getFormulationId() {
		return formulationId;
	}
	
	public void setFormulationId(Integer formulationId) {
		this.formulationId = formulationId;
	}
	
	public Brewing getNewBrewing() {
		return newBrewing;
	}
	
	public void setNewBrewing(Brewing newBrewing) {
		this.newBrewing = newBrewing;
	}
	
	public Boolean getImportButtonDisabled() {
		return importButtonDisabled;
	}
	
	public void setImportButtonDisabled(Boolean importButtonDisabled) {
		this.importButtonDisabled = importButtonDisabled;
	}
	
	public Boolean getCreateButtonDisabled() {
		return createButtonDisabled;
	}
	
	public void setCreateButtonDisabled(Boolean createButtonDisabled) {
		this.createButtonDisabled = createButtonDisabled;
	}
	
	@PostConstruct
	public void init() {
		newBrewing = new Brewing();
		createButtonDisabled = false;
		importButtonDisabled = true;
	}
	
	public void loadFormulation() {
		Formulation formulation = em.find(Formulation.class, formulationId);
		newBrewing.setFormulation(formulation);
	}
	
	public String createBrewing() {
		if (getWritable() == false) { return ""; }
		
		em.persist(newBrewing);
		return "/recipes/brewings/edit?faces-redirect=true&id=" + Integer.toString(newBrewing.getId());
	}
	
	public String importBrewing() {
		if (getWritable() == false) { return ""; }
		
		em.persist(newBrewing);
		renameTempBrewingFile();
		try {
			importer.copyFromBTP(newBrewing);
		} catch (BrewingDataImportException bdie) {
			log.error("Brewing import failed", bdie);
		}
		
		em.flush();
		return "/recipes/brewings/edit?faces-redirect=true&id=" + Integer.toString(newBrewing.getId());
	}
	
	public void uploadListener(FileUploadEvent event) {
		if (getWritable() == false) { return; }
		
		// Save to temp file if the recipe version dosen't have an ID yet
		File brewingFile = createTempBrewingFile();
		
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(brewingFile);
			fout.write(event.getFile().getContents());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Brewing file upload complete.", null));
			
			importButtonDisabled = false;
			createButtonDisabled = true;
		} catch (IOException ioe) {
			log.error("Failed to save upload to location: " + brewingFile.getAbsolutePath(), ioe);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upload failed.", null));
		} finally {
			try { fout.close(); } catch (IOException ioe2) {}
		}
	}
	
	public boolean getWritable() {
		if (session.getLoggedIn()) {
			if (session.getLoggedInUser().getBrewery().getId() == newBrewing.getFormulation().getRecipe().getBreweryId()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * It is expected that the new brewing has been persisted and thus has an ID before this method is called.
	 */
	protected void renameTempBrewingFile() {
		if (newBrewing.getId() == null) { throw new NullPointerException("newBrewing has no ID"); }
		
		if (tempBrewingFile != null && tempBrewingFile.exists()) {
			File brewingFile = new File(recipeFileLocation, "brewing-" + Integer.toString(newBrewing.getId()) + ".btp");
			
			if (brewingFile.exists()) {
				brewingFile.delete();
			}
			
			tempBrewingFile.renameTo(brewingFile);
		}
	}
	
	protected File createTempBrewingFile() {
		tempBrewingFile = new File(recipeFileLocation, "temp-brewing-" + UUID.randomUUID().toString() + ".btp");
		return tempBrewingFile;
	}
}
