package com.cervisium.webapp.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.RecipeFileLocation;
import com.cervisium.webapp.bean.model.IntegerKeyedModel;
import com.cervisium.webapp.btp.BTPData;
import com.cervisium.webapp.entity.Brewing;
import com.cervisium.webapp.entity.Formulation;
import com.cervisium.webapp.entity.FormulationNotes;
import com.cervisium.webapp.entity.Recipe;
import com.cervisium.webapp.services.RecipeDeleteService;

@Named("formulationEditor")
@ViewScoped
public class FormulationEditorBean implements Serializable {
	public static final long serialVersionUID = 1l;

	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject @RecipeFileLocation private File recipeFileLocation;
	
	@Inject SessionState session;
	@Inject RecipeDeleteService deleteService;
	
	private Integer newFormulationRecipeId;
	private Integer	editFormulationId;
	private Formulation editedFormulation;
	private BTPData btpData;
	
	private File tempFormulationFile;
	
	private String notes = "";
	
	public Integer getNewFormulationRecipeId() { return newFormulationRecipeId; }
	public void setNewFormulationRecipeId(Integer newFormulationRecipeId) { this.newFormulationRecipeId = newFormulationRecipeId; }
	public Formulation getEditedFormulation() { return editedFormulation; }
	public void setEditedFormulation(Formulation editedFormulation) { this.editedFormulation = editedFormulation; }
	public Integer getEditFormulationId() { return editFormulationId; }
	public void setEditFormulationId(Integer editFormulationId) { this.editFormulationId = editFormulationId; }
	public BTPData getBtpData() { return btpData; }
	public void setBtpData(BTPData btpData) { this.btpData = btpData; }
	public String getNotes() { return notes; }
	public void setNotes(String notes) { this.notes = notes; }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void loadFormulation() {
		if (editFormulationId != null) {
			if (editFormulationId == -1) {
				editedFormulation = new Formulation();
				Recipe recipe = em.find(Recipe.class, newFormulationRecipeId);
				editedFormulation.setRecipe(recipe);
			} else {
				editedFormulation = em.find(Formulation.class, editFormulationId);
				FormulationNotes notesRecord = em.find(FormulationNotes.class, editFormulationId);
				
				if (notesRecord != null) { notes = notesRecord.getNotes(); }
				else { notes = ""; }
			}
			
			if (editedFormulation.getFilePath() != null) {
				loadBtpData(new File(editedFormulation.getFilePath()));
			}
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveFormulation() {
		if (getWritable() == false) { return; }
		
		if (editedFormulation.getId() == null) {
			em.persist(editedFormulation);
			File formulationFile = renameTempFormulationFile();
			if (formulationFile != null) { editedFormulation.setFilePath(formulationFile.getAbsolutePath()); }
			em.flush();
		} else {
			em.merge(editedFormulation);
		}
		
		FormulationNotes notesRecord = new FormulationNotes();
		notesRecord.setId(editedFormulation.getId());
		notesRecord.setNotes(notes);
		em.merge(notesRecord);
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Formulation Saved.", null));
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String deleteFormulation() {
		if (getWritable() == false) { return ""; }
		
		String idStr = Integer.toString(editedFormulation.getRecipe().getId());
		deleteService.deleteForumlations(editedFormulation);
		return "/recipes/edit?faces-redirect=true&id=" + idStr;
	}
	
	public void uploadListener(FileUploadEvent event) {
		log.info("Recipe version file uploaded");
		if (getWritable() == false) { log.error("User without write access uploaded file."); return; }
		
		// Save to temp file if the recipe version dosen't have an ID yet
		File formulationFile = createFormulationFile();
		
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(formulationFile);
			fout.write(event.getFile().getContents());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Data file upload complete.", null));
		} catch (IOException ioe) {
			log.error("Failed to save upload to location: " + formulationFile.getAbsolutePath(), ioe);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upload failed.", null));
		} finally {
			try { fout.close(); } catch (IOException ioe2) {}
		}
		
		loadBtpData(formulationFile);
	}
	
	public boolean getWritable() {
		if (session.getLoggedIn()) {
			if (session.getLoggedInUser().getBrewery().getId() == editedFormulation.getRecipe().getBreweryId()) {
				return true;
			}
		}
		
		 return false;
	}
	
	protected File renameTempFormulationFile() {
		if (tempFormulationFile != null && tempFormulationFile.exists()) {
			File formulationFile = createFormulationFile();
			
			// If, somehow, the formulation file already exists, just roll with the temp file
			if (formulationFile.exists() == true) {
				log.error("Filename conflict. New formulation record " +
					Integer.toString(editedFormulation.getId()) +
					" created bult file already exists: " + formulationFile.getAbsolutePath());
				editedFormulation.setFilePath(tempFormulationFile.getAbsolutePath());
				tempFormulationFile = null;
			}
			// Normal behavior, rename temp file
			else {
				tempFormulationFile.renameTo(formulationFile);
				tempFormulationFile = null;
			}
			
			return formulationFile;
		}
		
		return null;
	}
	
	protected File createFormulationFile() {
		if (editedFormulation.getId() == null) {
			if (tempFormulationFile == null) {
				tempFormulationFile = new File(recipeFileLocation, "temp-formulation-" + UUID.randomUUID().toString() + ".btp");
			}
			
			return tempFormulationFile;
		} else {
			if (editedFormulation.getFilePath() == null) {
				File formulationFile = new File(recipeFileLocation, "formulation-" + Integer.toString(editedFormulation.getId()) + ".btp");
				editedFormulation.setFilePath(formulationFile.getAbsolutePath());
				return formulationFile;
			} else {
				return new File(editedFormulation.getFilePath());
			}
		}
	}
	
	protected void loadBtpData(File dataFile) {
		btpData = new BTPData(dataFile);
		try {
			btpData.loadDataFile();
		} catch (IOException ioe) {
			log.error("Loading BTP file failed: " + dataFile.getAbsolutePath(), ioe);
			btpData = null;
		}
	}
}
