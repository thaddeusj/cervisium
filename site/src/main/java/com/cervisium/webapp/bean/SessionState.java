package com.cervisium.webapp.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.entity.Brewery;
import com.cervisium.webapp.entity.BreweryUser;

@Named("sessionState")
@SessionScoped
public class SessionState implements Serializable {
	public static final long serialVersionUID = 1l;

	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	
	private BreweryUser loggedInUser;
	private Brewery activeBrewery;
	
	@PostConstruct
	public void init() {
		activeBrewery = em.createQuery("SELECT b FROM Brewery b WHERE b.name = :name", Brewery.class)
			.setParameter("name", "Cervisium")
			.getSingleResult();
		if (activeBrewery != null) {
			log.info("Loaded default brewery: " + activeBrewery.getName());
		} else {
			log.error("Failed to find default brewery Cervisium");
		}
		
		loggedInUser = null;
	}
	
	public Brewery getActiveBrewery() {
		return activeBrewery;
	}
	
	public void setActiveBrewery(Brewery activeBrewery) {
		this.activeBrewery = activeBrewery;
	}

	public boolean getLoggedIn() {
		return (loggedInUser != null);
	}
	
	public BreweryUser getLoggedInUser() {
		return loggedInUser;
	}
	
	public void setLoggedInUser(BreweryUser loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	
	public boolean getUserActiveBreweryMember() {
		if (getLoggedIn()) {
			if (getLoggedInUser().getBrewery().getId() == getActiveBrewery().getId()) {
				return true;
			}
		}
		
		return false;
	}
}
