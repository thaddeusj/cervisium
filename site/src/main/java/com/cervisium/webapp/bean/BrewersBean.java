package com.cervisium.webapp.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import com.cervisium.webapp.Cervisium;
import com.cervisium.webapp.bean.model.IntegerKeyedModel;
import com.cervisium.webapp.bean.model.PasswordChange;
import com.cervisium.webapp.entity.Brewer;
import com.cervisium.webapp.entity.BreweryUser;

@Named("brewers")
@ViewScoped
public class BrewersBean implements Serializable {
	public static final long serialVersionUID = 1l;
	
	@Inject @Cervisium private transient Logger log;
	@Inject @Cervisium private transient EntityManager em;
	@Inject private SessionState session;
	
	private Brewer newBrewer = new Brewer();
	private List<Brewer> brewers;
	private Brewer editedBrewer;
	private Integer selectedEditBrewerId;
	private SelectItem noBrewerSelectedItem = new SelectItem(new Integer(-1), "Select Brewer...");
	
	private IntegerKeyedModel<BreweryUser> breweryUsers;
	private BreweryUser selectedBreweryUser;
	private BreweryUser newBreweryUser;
	private PasswordChange passwordChange;
	private PasswordChange newUserPassword;
	
	private UIComponent changeNewPasswordConfirm;
	private UIComponent newUserPasswordConfirm;
	
	@PostConstruct
	public void init() {
		loadBrewerList();
		selectedEditBrewerId = -1;
		loadBreweryUsersList();
		resetNewBreweryUser();
	}
	
	public Brewer getNewBrewer() {
		return newBrewer;
	}
	
	public List<Brewer> getBrewers() {
		return brewers;
	}
	
	public Brewer getEditedBrewer() {
		return editedBrewer;
	}
	
	public void setEditedBrewer(Brewer editedBrewer) {
		this.editedBrewer = editedBrewer;
	}
	
	public Integer getSelectedEditBrewerId() {
		return selectedEditBrewerId;
	}
	
	public void setSelectedEditBrewerId(Integer selectedEditBrewerId) {
		this.selectedEditBrewerId = selectedEditBrewerId;
		editedBrewer = em.find(Brewer.class, selectedEditBrewerId);
		em.detach(editedBrewer);
	}
	
	public List<SelectItem> getBrewersItems() {
		ArrayList<SelectItem> items = new ArrayList<SelectItem>();
		for (Brewer b : getBrewers()) { items.add(new SelectItem(b.getId(), b.getName())); }
		return items;
	}
	
	public SelectItem getNoBrewerSelectedItem() {
		return noBrewerSelectedItem;
	}
	
	public IntegerKeyedModel<BreweryUser> getBreweryUsers() {
		return breweryUsers;
	}
	
	public void setBreweryUsers(IntegerKeyedModel<BreweryUser> breweryUsers) {
		this.breweryUsers = breweryUsers;
	}
	
	public BreweryUser getSelectedBreweryUser() {
		return selectedBreweryUser;
	}
	
	public void setSelectedBreweryUser(BreweryUser selectedBreweryUser) {
		this.selectedBreweryUser = selectedBreweryUser;
	}
	
	public BreweryUser getNewBreweryUser() {
		return newBreweryUser;
	}
	
	public void setNewBreweryUser(BreweryUser newBreweryUser) {
		this.newBreweryUser = newBreweryUser;
	}
	
	public PasswordChange getPasswordChange() {
		return passwordChange;
	}
	
	public void setPasswordChange(PasswordChange passwordChange) {
		this.passwordChange = passwordChange;
	}
	
	public UIComponent getChangeNewPasswordConfirm() {
		return changeNewPasswordConfirm;
	}
	
	public void setChangeNewPasswordConfirm(UIComponent changeNewPasswordConfirm) {
		this.changeNewPasswordConfirm = changeNewPasswordConfirm;
	}
	
	public PasswordChange getNewUserPassword() {
		return newUserPassword;
	}
	
	public void setNewUserPassword(PasswordChange newUserPassword) {
		this.newUserPassword = newUserPassword;
	}
	
	public UIComponent getNewUserPasswordConfirm() {
		return newUserPasswordConfirm;
	}
	
	public void setNewUserPasswordConfirm(UIComponent newUserPasswordConfirm) {
		this.newUserPasswordConfirm = newUserPasswordConfirm;
	}
	
	public void saveNewBrewer() {
		if (getBrewersWritable() == false) { return; }
		
		newBrewer.setBreweryId(session.getActiveBrewery().getId());
		em.persist(newBrewer);
		em.detach(newBrewer);
		brewers.add(newBrewer);
		newBrewer = new Brewer();
	}
	
	public void updateBrewer() {
		if (getBrewersWritable() == false) { return; }
		
		if (editedBrewer != null && selectedEditBrewerId >= 0) { em.merge(editedBrewer); }
		loadBrewerList();
	}
	
	public void deleteBrewer() {
		if (getBrewersWritable() == false) { return; }
		
		if (selectedEditBrewerId >= 0) {
			int deleted = em.createQuery("DELETE FROM Brewer b WHERE b.id = :id")
				.setParameter("id", editedBrewer.getId()).executeUpdate();
			if (deleted > 0) { loadBrewerList(); }
		}
		selectedEditBrewerId = -1;
		editedBrewer = null;
	}
	
	public boolean getBrewersWritable() {
		if (session.getUserActiveBreweryMember()) { return true; }
		return false;
	}
	
	public boolean getBreweryUsersWritable() {
		if (session.getLoggedIn()) {
			if (session.getLoggedInUser().getPrivUserAdmin()) {
				return true;
			}
		}
		
		return false;
	}
	
	public void selectBreweryUser() {
		passwordChange = new PasswordChange();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void changePassword() {
		if (getBreweryUsersWritable() == false) { return; }
		
		if (passwordChange.getNewPassword().equals(passwordChange.getNewPasswordConfirm()) == false) {
			FacesContext.getCurrentInstance().addMessage(changeNewPasswordConfirm.getClientId(),
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Must match New Password", null));
		} else {
			String newHash = BCrypt.hashpw(passwordChange.getNewPassword(), BCrypt.gensalt(12));
			em.createQuery("UPDATE BreweryUser bu SET bu.passHash = :newHash WHERE bu.id = :id")
				.setParameter("newHash", newHash).setParameter("id", selectedBreweryUser.getId())
				.executeUpdate();
			
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Password changed for " + selectedBreweryUser.getEmail(), null));
			
			passwordChange = new PasswordChange();
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createBreweryUser() {
		if (getBreweryUsersWritable() == false) { return; }
		
		if (newUserPassword.getNewPassword().equals(newUserPassword.getNewPasswordConfirm()) == false) {
			FacesContext.getCurrentInstance().addMessage(newUserPasswordConfirm.getClientId(),
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Must match New User Password", null));
		} else {
			newBreweryUser.setPassHash(BCrypt.hashpw(newUserPassword.getNewPassword(), BCrypt.gensalt(12)));
			newBreweryUser.setBrewery(session.getActiveBrewery());
			
			em.persist(newBreweryUser);
			em.detach(newBreweryUser);
			em.flush();
			
			loadBreweryUsersList();
			
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "New brewery user created: " + newBreweryUser.getEmail(), null));
			
			newBreweryUser = new BreweryUser();
			newUserPassword = new PasswordChange();
		}
	}
	
	public void resetNewBreweryUser() {
		newBreweryUser = new BreweryUser();
		newUserPassword = new PasswordChange();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteSelectedUser() {
		if (getBreweryUsersWritable() == false) { return; }
		
		if (selectedBreweryUser == null) {
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Delete requested but no user is selected", null));
		} else {
			em.createQuery("DELETE FROM BreweryUser bu WHERE bu.id = :id")
				.setParameter("id", selectedBreweryUser.getId()).executeUpdate();
			
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Brewery User deleted: " + selectedBreweryUser.getEmail(), null));
			
			selectedBreweryUser = null;
			
			loadBreweryUsersList();
		}
	}
	
	protected void loadBreweryUsersList() {
		List<BreweryUser> users = em.createQuery("SELECT bu FROM BreweryUser bu WHERE bu.brewery.id = :breweryId", BreweryUser.class)
			.setParameter("breweryId", session.getActiveBrewery().getId()).getResultList();
		for (BreweryUser bu : users) { em.detach(bu); bu.setPassHash(null); }
		breweryUsers = new IntegerKeyedModel<BreweryUser>(users);
	}
	
	protected void loadBrewerList() {
		brewers = em.createQuery("SELECT b FROM Brewer b WHERE b.breweryId = :breweryId", Brewer.class)
			.setParameter("breweryId", session.getActiveBrewery().getId())
			.getResultList();
	}
}
