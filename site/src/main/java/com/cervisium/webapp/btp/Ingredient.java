package com.cervisium.webapp.btp;

import java.util.Formatter;

public class Ingredient implements Comparable<Ingredient> {

	protected String name;
	protected IngredientStage stage;
	protected Float quantity;
	protected String quantityUnit;
	protected Float boilDuration;
	protected String boilDurationUnit;
	
	public String getName() {
		return name;
	}
	
	public IngredientStage getStage() {
		return stage;
	}
	
	public void setStage(IngredientStage stage) {
		this.stage = stage;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Float getQuantity() {
		return quantity;
	}
	
	public void setQuantity(Float quantity) {
		this.quantity = quantity;
	}
	
	public String getQuantityUnit() {
		return quantityUnit;
	}
	
	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}
	
	public String getDisplayQuantity() {
		return format("%.1f %s", getQuantity(), getQuantityUnit());
	}
	
	public Float getBoilDuration() {
		return boilDuration;
	}
	
	public void setBoilDuration(Float boilDuration) {
		this.boilDuration = boilDuration;
	}
	
	public String getBoilDurationUnit() {
		return boilDurationUnit;
	}
	
	public void setBoilDurationUnit(String boilDurationUnit) {
		this.boilDurationUnit = boilDurationUnit;
	}
	
	public String getDisplayBoilDuration() {
		if (getBoilDuration() == null || getBoilDuration() == 0f) { return ""; }
		else {return format("%.2f %s", getBoilDuration(), getBoilDurationUnit()); }
	}
	
	protected String format(String format, Object... values) {
		StringBuilder str = new StringBuilder();
		Formatter f = new Formatter(str);
		f.format(format, values);
		return str.toString();
	}
	
	public int getOrderPoints() {
		return 0;
	}
	
	@Override
	public int compareTo(Ingredient o) {
		int myPts = getOrderPoints();
		if (boilDuration != null) { myPts += (999 - boilDuration); }
		int oPts = o.getOrderPoints();
		if (o.getBoilDuration() != null) { oPts += (999 - o.getBoilDuration()); }
		
		return myPts - oPts;
	}
}
