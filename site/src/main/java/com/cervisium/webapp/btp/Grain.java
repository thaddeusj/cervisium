package com.cervisium.webapp.btp;

public class Grain extends Ingredient {
	@Override
	public int getOrderPoints() {
		return 1000;
	}
}
