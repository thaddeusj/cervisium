package com.cervisium.webapp.btp;

public class BrewingData {
	
	private Float og;
	private Float fg;
	private Float mashTempF;
	private Float efficiency;
	private Float attenuationApp;
	
	public Float getOg() {
		return og;
	}
	
	public void setOg(Float og) {
		this.og = og;
	}

	public Float getFg() {
		return fg;
	}

	public void setFg(Float fg) {
		this.fg = fg;
	}

	public Float getMashTempF() {
		return mashTempF;
	}

	public void setMashTempF(Float mashTempF) {
		this.mashTempF = mashTempF;
	}

	public Float getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(Float efficiency) {
		this.efficiency = efficiency;
	}

	public Float getAttenuationApp() {
		return attenuationApp;
	}

	public void setAttenuationApp(Float attenuationApp) {
		this.attenuationApp = attenuationApp;
	}
}
