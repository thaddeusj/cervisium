package com.cervisium.webapp.btp;

public enum IngredientStage {

	MASH(0, "Mash"),
	BOIL(2, "Boil"),
	LATE_ADDITION(3, "Late Add"),
	FERMENTER(4, "Fermenter"),
	UNKNOWN(-1, "Unknown");
	
	private final int value;
	private final String stageName;
	
	private IngredientStage(int value, String stageName) {
		this.value = value;
		this.stageName = stageName;
	}
	
	public int getValue() {
		return value;
	}
	
	public static IngredientStage forValue(int value) {
		for (IngredientStage is : values()) { if (is.getValue() == value) { return is; } }
		return IngredientStage.UNKNOWN;
	}
	
	@Override
	public String toString() {
		return stageName;
	}
}
